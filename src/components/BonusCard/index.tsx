import { FC } from 'react'

interface BonusCardProps {
  title: string
  freeSpinDesc: string
  imgPath: string
  children?: JSX.Element
  bgColor: string
  onOpen?: () => void
}

const BonusCard: FC<BonusCardProps> = ({
  title,
  freeSpinDesc,
  imgPath,
  children,
  bgColor,
  onOpen,
}) => {
  return (
    <div className={`welcome-bonus-wrapper ${bgColor}`}>
      <div className="chest-wrapper">
        <img width="74" src={imgPath} alt={title} />
      </div>
      <div className="welcome-bonus-description-wrapper">
        <h4 className="welcome-bonus-title">{title}</h4>
        <p className="free-spin-bonus">{freeSpinDesc}</p>
      </div>
      <div className="line-divider"></div>
      <div className="arrow-down-wrapper" onClick={onOpen}>
        {children}
      </div>
    </div>
  )
}

export default BonusCard
