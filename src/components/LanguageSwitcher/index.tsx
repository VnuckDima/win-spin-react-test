import './styles.css'

import { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import flagEn from 'src/assets/icons/flagEn.png'
import flagRu from 'src/assets/icons/flagRu.png'

const LanguageSwitcher = () => {
  const storedLanguage = localStorage.getItem('selectedLanguage')
  const initialLanguage = storedLanguage || 'flagEn'

  const { i18n } = useTranslation()

  const [selectedLanguage, setSelectedLanguage] = useState(initialLanguage)

  useEffect(() => {
    console.log('i18nextLng:', i18n.language)
    console.log('selectedLanguage:', selectedLanguage)
    if (localStorage) {
      localStorage.setItem('selectedLanguage', selectedLanguage)
    }

    i18n.changeLanguage(selectedLanguage)
  }, [i18n, selectedLanguage])

  const handleChangeLanguage = (language: string) => {
    setSelectedLanguage(language)
    i18n.changeLanguage(language)
  }

  const getFlagImage = () => {
    switch (selectedLanguage) {
      case 'en':
        return flagEn
      case 'ru':
        return flagRu
      default:
        return flagEn
    }
  }

  return (
    <div className="language-switcher">
      <span className="flag-icon">
        <img src={getFlagImage()} alt="Flag" />
      </span>
      <select
        value={selectedLanguage}
        onChange={(e) => handleChangeLanguage(e.target.value)}
        className="select-lang"
      >
        <option className="" value="en">
          EN
        </option>
        <option className="" value="ru">
          RU
        </option>
      </select>
    </div>
  )
}

export default LanguageSwitcher
