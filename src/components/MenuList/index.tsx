import './styles.css'

import { FC } from 'react'

interface MenuListProps {
  title: string
  icon: string
  width: string
}

const MenuList: FC<MenuListProps> = ({ title, icon, width }) => {
  return (
    <div className="menu-card">
      <div>
        <img width={width} src={icon} alt={title} />
      </div>
      <h4 className="card-title">{title}</h4>
    </div>
  )
}

export default MenuList
