import { FC, ReactNode } from 'react'
import ReactDOM from 'react-dom'

interface PortalProps {
  children: ReactNode
}

const Portal: FC<PortalProps> = ({ children }) => {
  const portalRoot = document.getElementById('root')

  if (!portalRoot) {
    throw new Error('No portal root found')
  }

  return ReactDOM.createPortal(children, portalRoot)
}

export default Portal
