import './styles.css'

import { FC } from 'react'

interface MarqueeContProps {
  imgGame: string
  title: string
  description: string
}

const MarqueeCont: FC<MarqueeContProps> = ({ imgGame, title, description }) => {
  return (
    <div className="marquee-wrapper">
      <div>
        <p>{title}</p>
        <p className="description_color-yellow">{description}</p>
      </div>
      <div>
        <img width="50px" src={imgGame} alt={title} />
      </div>
    </div>
  )
}

export default MarqueeCont
