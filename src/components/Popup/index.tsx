import './styles.css'

import { FC, ReactNode, useEffect, useRef } from 'react'

import Portal from '../Portal'

interface PopupProps {
  onClose: () => void
  children: ReactNode
}

const Popup: FC<PopupProps> = ({ onClose, children }) => {
  const popupRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    const handleOutsideClick = (event: MouseEvent) => {
      if (popupRef.current && !popupRef.current.contains(event.target as Node)) {
        onClose()
      }
    }

    document.addEventListener('mousedown', handleOutsideClick)

    return () => {
      document.removeEventListener('mousedown', handleOutsideClick)
    }
  }, [onClose])

  return (
    <Portal>
      <div className="popup" ref={popupRef}>
        <div>{children}</div>
      </div>
    </Portal>
  )
}

export default Popup
