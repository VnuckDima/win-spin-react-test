import './styles.css'

import { FC } from 'react'
import { useTranslation } from 'react-i18next'
import Menu from 'src/assets/icons/Menu'

import Button from '../Common/Button'

interface FooterProps {
  onClick: () => void
}

const Footer: FC<FooterProps> = ({ onClick }) => {
  const { t } = useTranslation()

  return (
    <footer className="footer-wrapper">
      <div>
        <Button className="sign-btn sign-in-btn size-btn">{t('Sign in')}</Button>
      </div>
      <div>
        <Menu className="menu-icon" onClick={onClick} />
      </div>
      <div>
        <Button className="sign-btn sign-up-btn size-btn">{t('Sign up')}</Button>
      </div>
    </footer>
  )
}

export default Footer
