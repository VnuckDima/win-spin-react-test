import { ChangeEvent, useState } from 'react'

import Spinner from '../Spinner'

interface SelectProps {
  isLoading: boolean
  spinnerWrapperClassName: string
  optionClassName: string
  options: { name: string; code: number }[]
  defaultValue?: string
  className?: string
  placeholder?: string
  onChange: (selectedValue: string) => void
}

const Select: React.FC<SelectProps> = ({
  isLoading,
  spinnerWrapperClassName,
  optionClassName,
  options,
  defaultValue,
  className,
  placeholder,
  onChange,
}) => {
  const [selectedValue, setSelectedValue] = useState<string>(defaultValue || '')

  const handleSelectChange = (e: ChangeEvent<HTMLSelectElement>) => {
    const value = e.target.value
    setSelectedValue(value)
    onChange(value)
  }

  return (
    <>
      {isLoading ? (
        <div className={spinnerWrapperClassName}>
          <Spinner />
        </div>
      ) : (
        <select value={selectedValue} onChange={handleSelectChange} className={className}>
          {placeholder && (
            <option value="" disabled hidden>
              {placeholder}
            </option>
          )}
          {options.map((option) => (
            <option key={option.code} value={option.name} className={optionClassName}>
              {option.name}
            </option>
          ))}
        </select>
      )}
    </>
  )
}

export default Select
