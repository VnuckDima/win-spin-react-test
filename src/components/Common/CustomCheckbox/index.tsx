import './styles.css'

import CheckboxIcon from 'src/assets/icons/CheckboxIcon'

interface CheckboxProps {
  label: string
  checked: boolean
  onChange: (id: number) => void
  id: number
  name: string
  selectedId: number | null
}

const Checkbox: React.FC<CheckboxProps> = ({ label, checked, onChange, name, id, selectedId }) => {
  const handleCheckboxChange = () => {
    if (!checked) {
      onChange(id)
    }
  }

  return (
    <label className="custom-checkbox">
      <input type="radio" name={name} checked={selectedId === id} onChange={handleCheckboxChange} />
      <span className="custom-checkbox-icon">{checked && <CheckboxIcon />}</span>
      {label}
    </label>
  )
}

export default Checkbox
