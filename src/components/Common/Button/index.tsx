import React from 'react'

interface ButtonProps {
  isDisabled?: boolean
  className?: string
  children: JSX.Element | string
  onClick?: () => void
}

const Button: React.FC<ButtonProps> = ({ children, ...props }) => {
  return <button {...props}>{children}</button>
}

export default Button
