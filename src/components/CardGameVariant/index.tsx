import './styles.css'

import { FC } from 'react'

interface CardGameVariantProps {
  imgIcon: string
  width: string
  height: string
  title: string
  optionalCard: string
}

const CardGameVariant: FC<CardGameVariantProps> = ({
  imgIcon,
  width,
  height,
  title,
  optionalCard,
}) => {
  return (
    <div className="card-wrapper">
      <div className={`card-wrapper-title-with-img ${optionalCard}-card`}>
        <div>
          <img width={width} height={height} src={imgIcon} alt={title} />
        </div>
        <p>{title}</p>
      </div>
    </div>
  )
}

export default CardGameVariant
