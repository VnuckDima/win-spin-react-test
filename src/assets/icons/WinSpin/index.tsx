import { SVGProps } from 'react'

const WinSpin = (props?: SVGProps<SVGSVGElement>) => {
  return (
    <svg
      width="102.000000"
      height="54.450012"
      viewBox="0 0 102 54.45"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink={props?.xmlnsXlink}
      {...props}
    >
      <defs>
        <linearGradient
          id="paint_linear_1_1388_0"
          x1="20.200001"
          y1="10.521149"
          x2="20.200001"
          y2="52.474995"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#F2843F" />
          <stop offset="1.000000" stopColor="#FFD81E" />
        </linearGradient>
      </defs>
      <path
        id="Union"
        d="M20.2 10.5211C9.04385 10.5211 0 19.3911 0 30.3327L0 49.5615C0 51.1706 1.32997 52.475 2.97059 52.475L8.31764 52.475C11.9099 52.475 13.7057 50.3798 15.3749 47.7186C17.8629 43.7522 22.5371 43.7522 25.0251 47.7186C26.6943 50.3798 28.4901 52.475 32.0824 52.475L37.4294 52.475C39.07 52.475 40.4 51.1706 40.4 49.5615L40.4 30.3327C40.4 19.3911 31.3562 10.5211 20.2 10.5211Z"
        fill="url(#paint_linear_1_1388_0)"
        fillOpacity="1.000000"
        fillRule="evenodd"
      />
      <ellipse
        id="Ellipse 15"
        cx="20.199997"
        cy="28.390381"
        rx="8.546154"
        ry="7.769231"
        fill="#FFFFFF"
        fillOpacity="1.000000"
      />
      <circle
        id="Ellipse 16"
        cx="24.473083"
        cy="28.001923"
        r="2.719231"
        fill="#221B43"
        fillOpacity="1.000000"
      />
      <rect
        id="Rectangle 221"
        x="8.546158"
        y="6.636536"
        width="2.330769"
        height="10.100000"
        fill="#AFF860"
        fillOpacity="1.000000"
      />
      <rect
        id="Rectangle 222"
        x="30.300003"
        y="6.636536"
        width="2.330769"
        height="10.100000"
        fill="#AFF860"
        fillOpacity="1.000000"
      />
      <circle
        id="Ellipse 18"
        cx="9.711533"
        cy="5.471161"
        r="3.496154"
        fill="#AFF860"
        fillOpacity="1.000000"
      />
      <circle
        id="Ellipse 19"
        cx="31.846863"
        cy="5.471161"
        r="3.496154"
        fill="#AFF860"
        fillOpacity="1.000000"
      />
      <path
        id="Vector"
        d="M92.4597 33.96C92.4597 36.62 92.5498 38.96 92.7298 40.98C92.8298 42.22 92.9698 43.14 93.1497 43.74L95.1598 32.34L100.2 31.38C98.1797 39.12 96.8498 45.91 96.2097 51.75L91.5898 53.04L91.1998 52.02L89.6698 47.67C89.3698 46.83 89.2198 46.38 89.2198 46.32C88.8598 48.34 88.5397 50.58 88.2598 53.04L83.5798 54.45L87.4498 32.4L92.5198 31.38C92.4798 32.18 92.4597 33.04 92.4597 33.96Z"
        fill="#FFFFFF"
        fillOpacity="1.000000"
        fillRule="nonzero"
      />
      <path
        id="Vector"
        d="M79.4458 32.34L84.4858 31.38C82.4658 39.12 81.1358 45.91 80.4958 51.75L75.8158 53.04L79.4458 32.34Z"
        fill="#FFFFFF"
        fillOpacity="1.000000"
        fillRule="nonzero"
      />
      <path
        id="Vector"
        d="M73.5546 30.93C74.8746 30.93 75.8546 31.16 76.4946 31.62C77.1546 32.06 77.4746 32.74 77.4546 33.66C77.3946 37.34 74.2946 41.16 68.1546 45.12C67.7146 47.62 67.3946 49.83 67.1946 51.75L62.5446 53.04C62.6646 52.18 62.8946 50.87 63.2346 49.11L64.5246 42.18C65.6646 35.78 66.1946 32.49 66.1146 32.31C69.1146 31.39 71.5946 30.93 73.5546 30.93ZM72.7446 37.02C72.7446 36.68 72.4946 36.51 71.9946 36.51C71.5946 36.51 70.8646 36.67 69.8046 36.99C69.7446 37.21 69.6746 37.53 69.5946 37.95C69.5146 38.35 69.4646 38.58 69.4446 38.64L69.1146 40.23C70.8946 39.35 72.0646 38.4 72.6246 37.38C72.7046 37.24 72.7446 37.12 72.7446 37.02Z"
        fill="#FFFFFF"
        fillOpacity="1.000000"
        fillRule="nonzero"
      />
      <path
        id="Vector"
        d="M55.8068 45.33C55.5068 45.33 54.7068 45.45 53.4068 45.69C52.1068 45.91 51.3068 46.02 51.0068 46.02C50.0868 46.02 49.4068 45.77 48.9668 45.27C48.5068 44.77 48.3268 44.09 48.4268 43.23C48.6668 41.05 50.0568 38.54 52.5968 35.7C55.1768 32.8 57.4168 31.27 59.3168 31.11C61.1368 30.99 62.0468 32.35 62.0468 35.19C62.0468 35.77 61.9968 36.54 61.8968 37.5C61.7168 37.56 61.3768 37.69 60.8768 37.89C60.3968 38.09 59.9968 38.25 59.6768 38.37L57.4568 39.21C57.4568 38.29 57.2068 37.83 56.7068 37.83C56.1068 37.83 55.4568 38.16 54.7568 38.82C54.0768 39.48 53.7868 40.15 53.8868 40.83C53.9468 41.19 54.3168 41.37 54.9968 41.37C55.3368 41.37 55.9568 41.3 56.8568 41.16C57.7568 41.02 58.3668 40.95 58.6868 40.95C59.7468 40.95 60.5068 41.17 60.9668 41.61C61.4468 42.05 61.6868 42.76 61.6868 43.74C61.6868 44.98 61.2168 46.33 60.2768 47.79C59.0768 49.67 57.3768 51.26 55.1768 52.56C54.0168 53.26 52.8868 53.61 51.7868 53.61C50.7868 53.61 49.7868 53.32 48.7868 52.74C49.3068 51.5 50.0868 49.77 51.1268 47.55C51.7868 47.99 52.4568 48.21 53.1368 48.21C53.8968 48.21 54.7268 47.99 55.6268 47.55C56.5468 47.09 57.0068 46.6 57.0068 46.08C57.0068 45.58 56.6068 45.33 55.8068 45.33Z"
        fill="#FFFFFF"
        fillOpacity="1.000000"
        fillRule="nonzero"
      />
      <path
        id="Vector"
        d="M88.3078 3.95999C88.3078 6.62 88.3978 8.95999 88.5778 10.98C88.6778 12.22 88.8178 13.14 88.9978 13.74L91.0078 2.34L96.0478 1.38C94.0278 9.12 92.6978 15.91 92.0578 21.75L87.4378 23.04L87.0478 22.02L85.5178 17.67C85.2178 16.83 85.0678 16.38 85.0678 16.32C84.7078 18.34 84.3878 20.58 84.1078 23.04L79.4278 24.45L83.2978 2.39999L88.3678 1.38C88.3278 2.17999 88.3078 3.04001 88.3078 3.95999Z"
        fill="#FFFFFF"
        fillOpacity="1.000000"
        fillRule="nonzero"
      />
      <path
        id="Vector"
        d="M75.2939 2.34L80.3339 1.38C78.3139 9.12 76.9839 15.91 76.3439 21.75L71.6639 23.04L75.2939 2.34Z"
        fill="#FFFFFF"
        fillOpacity="1.000000"
        fillRule="nonzero"
      />
      <path
        id="Vector"
        d="M73.3087 1.20001C70.6887 8.12 68.7287 14.94 67.4287 21.66L60.8587 22.77L61.9387 12.03L59.2087 21.99L52.9687 23.13C53.9087 15.63 54.3687 8.89001 54.3487 2.91L59.9287 1.85999L58.7887 14.7L62.5687 0.450012L65.5687 0L63.5587 18.09L67.6087 2.10001L73.3087 1.20001Z"
        fill="#FFFFFF"
        fillOpacity="1.000000"
        fillRule="nonzero"
      />
    </svg>
  )
}

export default WinSpin
