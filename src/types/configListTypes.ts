export interface BonusCardItem {
  id: number
  title: string
  freeSpinDesc: string
  bgColor: string
  imgPath: string
}

export interface CardConfigItem {
  id: number
  title: string
  width: string
  height: string
  icon: string
  bgVariant: string
}

export interface MenuListItem {
  id: number
  title: string
  imgList: string
  width: string
}
