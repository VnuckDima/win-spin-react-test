import fruits from 'src/assets/images/slider-img/fruits.png'
import horsemen from 'src/assets/images/slider-img/horsemen.png'
import sun from 'src/assets/images/slider-img/sun.png'

export const marqueeConfig = [
  {
    id: 1,
    title: 'Robert - €18.75',
    imgGame: sun,
    description: 'in Sun of Egypt',
  },
  {
    id: 2,
    title: 'Robert - €18.75',
    imgGame: fruits,
    description: 'in Crystal Fruits',
  },
  {
    id: 3,
    title: 'Robert - €18.75',
    imgGame: horsemen,
    description: 'in 4 Horsemen',
  },
]
