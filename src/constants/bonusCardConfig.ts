import chest from 'src/assets/images/bonus-card-images/chest.png'
import deposit from 'src/assets/images/bonus-card-images/deposit-coins.png'
import noBonus from 'src/assets/images/bonus-card-images/no-bonus-chest.png'

export const bonusCardConfig = [
  {
    id: 1,
    title: 'Welcome bonus',
    freeSpinDesc: '100% up to €500 +120 FreeSpins',
    imgPath: chest,
    bgColor: 'red-bg',
  },
  {
    id: 2,
    title: 'First Deposit Bonus',
    freeSpinDesc: '100% up to €500 +120 FreeSpins',
    imgPath: deposit,
    bgColor: 'purple-bg',
  },
  {
    id: 3,
    title: 'No Bonus',
    freeSpinDesc: 'I do not want a Welcome bonus',
    imgPath: noBonus,
    bgColor: 'blue-bg',
  },
]
