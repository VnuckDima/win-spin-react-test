import dealers from 'src/assets/images/menu-list/dealers.png'
import hall from 'src/assets/images/menu-list/hall.png'
import lottery from 'src/assets/images/menu-list/lottery.png'
import paper from 'src/assets/images/menu-list/paper.png'
import payment from 'src/assets/images/menu-list/payment.png'
import shield from 'src/assets/images/menu-list/shield.png'
import skull from 'src/assets/images/menu-list/skull.png'
import support from 'src/assets/images/menu-list/support.png'
import vip from 'src/assets/images/menu-list/vip.png'

export const menuList = [
  {
    id: 1,
    title: 'Games',
    imgList: skull,
    width: '25',
  },
  {
    id: 2,
    title: 'Promotions',
    imgList: paper,
    width: '23',
  },
  {
    id: 3,
    title: 'Tournaments',
    imgList: shield,
    width: '25',
  },
  {
    id: 4,
    title: 'Live dealers',
    imgList: dealers,
    width: '25',
  },
  {
    id: 5,
    title: 'Lottery',
    imgList: lottery,
    width: '27',
  },
  {
    id: 6,
    title: 'Hall of fame',
    imgList: hall,
    width: '25',
  },
  {
    id: 7,
    title: 'VIP',
    imgList: vip,
    width: '25',
  },
  {
    id: 8,
    title: 'Payment',
    imgList: payment,
    width: '24',
  },
  {
    id: 9,
    title: 'Support',
    imgList: support,
    width: '23',
  },
]
