export const moneyList = [
  {
    code: 1,
    name: 'Argentine Peso',
  },
  {
    code: 2,
    name: 'Australian Dollar',
  },
  {
    code: 3,
    name: 'Baht',
  },
  {
    code: 4,
    name: 'Brazillian Real',
  },
  {
    code: 5,
    name: 'Bulgarian Lev',
  },
]
