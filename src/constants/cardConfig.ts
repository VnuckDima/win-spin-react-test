import liveCasino from 'src/assets/icons/card-icons/live-casino-icon.png'
import paper from 'src/assets/icons/card-icons/paper-icon.png'
import shield from 'src/assets/icons/card-icons/shield-and-sword-icon.png'
import skull from 'src/assets/icons/card-icons/skull-icon.png'

export const cardConfig = [
  {
    id: 1,
    title: 'Game slots',
    icon: skull,
    bgVariant: 'green',
    width: '35',
    height: '35',
  },
  {
    id: 2,
    title: 'Live casino',
    icon: liveCasino,
    bgVariant: 'red',
    width: '23',
    height: '30',
  },
  {
    id: 3,
    title: 'Promotions',
    icon: paper,
    bgVariant: 'gray',
    width: '25',
    height: '25',
  },
  {
    id: 4,
    title: 'Tournaments',
    icon: shield,
    bgVariant: 'purple',
    width: '28',
    height: '30',
  },
]
