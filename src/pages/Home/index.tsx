import './styles.css'

import { useEffect, useMemo, useRef, useState } from 'react'
import Marquee from 'react-fast-marquee'
import { useTranslation } from 'react-i18next'
import ArrowDown from 'src/assets/icons/ArrowDown'
import Flame from 'src/assets/icons/Flame'
import Scroll from 'src/assets/icons/Scroll'
import searchHeader from 'src/assets/icons/search-header.svg'
import WinSpin from 'src/assets/icons/WinSpin'
import chest from 'src/assets/images/bonus-card-images/chest.png'
import flag from 'src/assets/images/slider-img/flag.png'
import BonusCard from 'src/components/BonusCard'
import CardGameVariant from 'src/components/CardGameVariant'
import Button from 'src/components/Common/Button'
import Checkbox from 'src/components/Common/CustomCheckbox'
import Select from 'src/components/Common/Select'
import Footer from 'src/components/Footer'
import LanguageSwitcher from 'src/components/LanguageSwitcher'
import MarqueeCont from 'src/components/MarqueeCont'
import MenuList from 'src/components/MenuList'
import Popup from 'src/components/Popup'
import { bonusCardConfig } from 'src/constants/bonusCardConfig'
import { cardConfig } from 'src/constants/cardConfig'
import { marqueeConfig } from 'src/constants/marqueeConfig'
import { menuList } from 'src/constants/menuList'
import { moneyList } from 'src/constants/moneyList'
import { BonusCardItem, CardConfigItem, MenuListItem } from 'src/types/configListTypes'
import { fetchCountries } from 'src/utils/api'

const Home = () => {
  const [countries, setCountries] = useState([])
  const [loading, setLoading] = useState<boolean>(true)
  const [isPopupOpen, setIsPopupOpen] = useState(false)
  const [selectedId, setSelectedId] = useState<number | null>(null)
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const [searchTerm, setSearchTerm] = useState('')
  const inputRef = useRef<HTMLInputElement>(null)

  const { t } = useTranslation()

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await fetchCountries()
        setCountries(data)
        setLoading(false)
      } catch (error) {
        setLoading(false)

        console.error('Error fetching countries:', error)
      }
    }
    fetchData()
  }, [])

  const handleIconClick = () => {
    if (inputRef.current) {
      inputRef.current.focus()
    }
  }

  const handleSelectChange = (selectedValue: string) => {
    console.log('Selected value:', selectedValue)
  }

  const openPopup = () => {
    setIsPopupOpen(!isPopupOpen)
  }

  const handleCheckboxChange = (id: number | null) => () => {
    setSelectedId(id)
  }

  const handleMenuClick = () => {
    setIsMenuOpen(!isMenuOpen)
  }

  const handleSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(e.target.value)
  }

  const translateItems = useMemo(
    () =>
      <T extends { title: string }>(items: T[]) =>
        items.map((item) => ({ ...item, title: t(item.title) })),
    [t],
  )

  const translatedBonusCardConfig: BonusCardItem[] = translateItems(bonusCardConfig)

  const selectedBonusCard: BonusCardItem | undefined = translatedBonusCardConfig.find(
    (item) => item.id === selectedId,
  )

  const translatedCardConfig: CardConfigItem[] = translateItems(cardConfig)

  const translatedFilteredMenuList: MenuListItem[] = translateItems(menuList)

  const filteredMenuList = useMemo(
    () =>
      translatedFilteredMenuList.filter((item) =>
        item.title.toLowerCase().includes(searchTerm.toLowerCase()),
      ),
    [translatedFilteredMenuList, searchTerm],
  )

  const { title, freeSpinDesc, bgColor, imgPath } = useMemo(
    () => (selectedBonusCard ? selectedBonusCard : translatedBonusCardConfig[0]),
    [selectedBonusCard, translatedBonusCardConfig],
  )

  return (
    <>
      <div className="wrapper">
        <div className="wrapper__content-wrapper">
          <main className="home-wrapper">
            <WinSpin className="win-spin-icon" />
            <div className="search-spin-icon" onClick={handleMenuClick}>
              <img src={searchHeader} alt="search" />
            </div>

            <div className="home-wrapper__content content_bg-under-flame stylish-border">
              <div className="flame-svg-wrapper">
                <Flame className="flame-svg" />
              </div>

              <div className="rectangle">
                <h1 className="deposit-and-play">{t('Deposit and play')}</h1>
                {selectedBonusCard ? (
                  <BonusCard
                    title={title}
                    freeSpinDesc={freeSpinDesc}
                    imgPath={imgPath}
                    bgColor={bgColor}
                    onOpen={openPopup}
                  >
                    <div className="arrow-down-cursor">
                      <ArrowDown />
                    </div>
                  </BonusCard>
                ) : (
                  <BonusCard
                    title={t('Welcome bonus')}
                    freeSpinDesc={t('100% up to €500 +120 FreeSpins')}
                    imgPath={chest}
                    onOpen={openPopup}
                    bgColor={'red'}
                  >
                    <div className="arrow-down-cursor">
                      <ArrowDown />
                    </div>
                  </BonusCard>
                )}

                {isPopupOpen && (
                  <Popup onClose={openPopup}>
                    {translatedBonusCardConfig.map(
                      ({ id, title, freeSpinDesc, imgPath, bgColor }) => (
                        <BonusCard
                          key={id}
                          title={title}
                          freeSpinDesc={freeSpinDesc}
                          imgPath={imgPath}
                          bgColor={bgColor}
                        >
                          <Checkbox
                            label=""
                            checked={id === selectedId}
                            onChange={handleCheckboxChange(id)}
                            id={id}
                            name="bonus"
                            selectedId={selectedId}
                          />
                        </BonusCard>
                      ),
                    )}
                  </Popup>
                )}

                <div className="selects-wrapper">
                  <div>
                    <Select
                      spinnerWrapperClassName="spinner-wrapper"
                      optionClassName="option-wrapper"
                      isLoading={loading}
                      options={countries && countries}
                      defaultValue=""
                      placeholder={t('Country')}
                      className="select-value"
                      onChange={handleSelectChange}
                    />
                  </div>
                  <div>
                    <Select
                      spinnerWrapperClassName="spinner-wrapper"
                      optionClassName="option-wrapper"
                      isLoading={loading}
                      options={moneyList}
                      defaultValue=""
                      className="select-value"
                      placeholder="EUR"
                      onChange={handleSelectChange}
                    />
                  </div>
                </div>

                <div>
                  <Button className="start-button">{t('Start with your 100% bonus')}</Button>
                </div>
              </div>
            </div>

            <div className="game-variants-wrapper">
              {translatedCardConfig.map(({ id, title, width, height, icon, bgVariant }) => {
                return (
                  <CardGameVariant
                    key={id}
                    width={width}
                    height={height}
                    title={title}
                    imgIcon={icon}
                    optionalCard={bgVariant}
                  />
                )
              })}
            </div>

            <div className="slider-wrapper">
              <div className="recent-winners">
                <div>
                  <img width="60" src={flag} alt="flag image" />
                </div>
                <h5>
                  <p>{t('Recent')}</p>
                  <p className="winners_color-yellow">{t('Winners')}</p>
                </h5>
              </div>

              <Marquee className="marquee-font">
                {marqueeConfig.map(({ id, title, imgGame, description }) => {
                  return (
                    <MarqueeCont
                      key={id}
                      imgGame={imgGame}
                      title={title}
                      description={description}
                    />
                  )
                })}
              </Marquee>
            </div>

            {isMenuOpen && (
              <div className="fullscreen-menu">
                <div className="header-menu-wrapper">
                  <div className="header-search-wrapper">
                    <div>
                      <WinSpin className="win-spin-menu" />
                    </div>
                    <div className="search-icon" onClick={handleIconClick}>
                      <img src={searchHeader} alt="search" />
                    </div>
                  </div>
                  <div className="language-wrapper">
                    <div>
                      <Scroll />
                    </div>
                    <LanguageSwitcher />
                  </div>
                </div>

                <div className="footer-wrapper">
                  <div>
                    <Button className="sign-btn register-btn-header">{t('Register')}</Button>
                  </div>
                  <div>
                    <Button className="sign-btn log-in-btn-header">{t('Login')}</Button>
                  </div>
                </div>

                <div className="search-wrapper">
                  <input
                    ref={inputRef}
                    className="search"
                    placeholder={t('Game name or provider')}
                    value={searchTerm}
                    onChange={handleSearchChange}
                  />
                </div>

                <div className="list-wrapper">
                  {filteredMenuList.map(({ id, title, imgList, width }) => {
                    return <MenuList key={id} title={title} icon={imgList} width={width} />
                  })}
                </div>
              </div>
            )}
            <Footer onClick={handleMenuClick} />
          </main>
        </div>
      </div>
    </>
  )
}

export default Home
