export const fetchCountries = async () => {
  try {
    const response = await fetch(
      'https://cors-anywhere.herokuapp.com/https://moyaposylka.ru/api/v1/countries',
    )

    if (!response.ok) {
      throw new Error('Network response was not ok')
    }

    const data = await response.json()

    return data
  } catch (error) {
    console.error('ERROR_FETCHING_DATA: ', error)
    throw error
  }
}
